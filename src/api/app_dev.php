<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$app = new MyApp();

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : []);
    }
});
$app->post('api/message/{channel}', function (Request $request, $channel) use ($app) {

    $event = new MessageEvent();

    $event->setChannel(
        $app->escape($channel)
    );

    $event->setMessage(
        $app->escape(
            $request->request->get('message')
        )
    );

    $app['dispatcher']->dispatch('message.created', $event);

    return new JsonResponse($event->getId(), 201);
})
    ->value('channel', 'general')
    ->assert('channel', '^[a-zA-Z0-9]+$');

$app->forwardEventToPushServer('message.created');

$app->run();
